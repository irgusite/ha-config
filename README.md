# Willy Smart Home Documentation

This repo contains all my home automation config. It is all integrated in my appartement to automate as much as possible my (our) day! 

[![Build Status](https://travis-ci.org/irgusite/HA-Config.svg?branch=master)](https://travis-ci.org/irgusite/HA-Config)

## Hardware Used/Connected
Here is a non exhaustiv list of all the hardware on which my appartement runs :)

#### Foremost the IT hardware on which all the magic happens:
* Proxmox Server :
	* Intel Xeon E5620 quad core CPU (2.4GHz)
	* Asus P6T Deluxe Motherboard (yeah, not very server-ish)
	* 24Gb DDR3 Ram (some ECC ram waiting for a compatible motherboard)
	* Server Case (16 3.5" bays)

* Raspberry Pi 2 B:
	* Razberry Zwave Dongle

* Some networking gear 

#### Security
* Unifi G3-Flex Camera
* Somfy Home Protect (meh)

#### Lights
For the lights, all of them are Ikea tradfri bulbs or pannels as these are really cheap, color changing smart bulbs. 
Everything is connected to a zigbee2mqtt instance via a home flashed zigbee dongle. Cheap, and working really well. 

|[Ikea Floalt](https://www.ikea.com/ch/fr/p/floalt-panneau-eclairage-led-intensite-lumineuse-reglable-spectre-blanc-20436317/)|[Ikea Gunnarp](https://www.ikea.com/ch/fr/p/gunnarp-plafonnier-applique-blanc-intensite-lumineuse-reglable-spectre-blanc-50360071/)|[Ikea TRÅDFRI 1000 Lumen](https://www.ikea.com/ch/fr/p/tradfri-ampoule-led-e27-1000-lumen-sans-fil-a-variateur-dintensite-spectre-blanc-opalin-60408483/)|
|:----------:|:---------------:|:-----------------------------:|
|L1527 L1528 & L1529 FLOALT|T1829 GUNNARP|LED1732G11|
|![Ikea Floalt](https://www.ikea.com/ch/fr/images/products/floalt-led-light-panel-dimmable-white-spectrum__0685698_PE721474_S5.JPG?f=s)|![Ikea Gunnarp](https://www.ikea.com/ch/fr/images/products/gunnarp-led-ceiling-wall-lamp__0722223_PE733534_S5.JPG?f=s)|![Ikea tradfri](https://www.ikea.com/ch/fr/images/products/tradfri-led-bulb-e27-1000-lumen__0609096_PE684323_S5.JPG?f=s)|


#### Switches
|Aeon Labs Gen5 Switch|Ikea Tradfri switch|Shelly 1|Fibaro FGS221|
|:-------------------:|:-----------------:|:------:|:-----------:|
|![Aeonlabs switch](https://products.z-wavealliance.org/ProductImages/Index?productName=ZC10-14090009)|![Ikea Switch](https://www.ikea.com/ch/fr/images/products/tradfri-wireless-control-outlet__0515598_PE640312_S5.JPG?f=s)|![Shelly module](https://shop-cdn-1.shelly.cloud/image/cache/catalog/shelly_1/s1_x1-1000x1000.jpg)|![Fibaro double relay](https://manuals.fibaro.com/wp-content/uploads/2017/02/drs_icon.png)

Most of the switches are used for ambiance lights or accesories. The HTC Vive lighthouses are connected each to a shelly 1 to turn them on when using the VR headset. The Aeotec switch is used for the primary livingroom spot, and is working very well. 

#### Vacuum
The vacuum used in the home is a Xiaomi Rockrobo 1 (First gen). It is flashed with [Valetudo](https://github.com/Hypfer/Valetudo) to be 100% local used. It is connected to Home Assistant Via MQTT.

#### Sensors

|Aqara Humidity and Temperature sensor|ESP 8266|Fibaro Motion detector|Ikea TRÅDFRI Motion Sensor|
|:-----:|:----:|:----:|:----:|
|![Aqara Temp sensor](https://s2.qwant.com/thumbr/0x380/c/6/1cee432c62d49a0dae6914e8360ba7ff25af5902a196d216d7194a1b29960a/_D.jpg?u=https%3A%2F%2Fstatic.turbosquid.com%2FPreview%2F001267%2F450%2F7J%2F_D.jpg&q=0&b=1&p=0&a=1)|![ESP8266](https://imgaz.staticbg.com/thumb/grid/oaupload/banggood/images/67/10/0e04f320-e4f9-45bb-a24e-0b66a9000dee.JPG.webp)|![Fibaro Motion sensor](https://s2.qwant.com/thumbr/0x380/c/5/4e5a6d407fb52b6d5e205248416f57dde837d1100c68bf216f41e7a16b2f3b/fibaro-motion-sensor.jpg?u=http%3A%2F%2Fsmarta.ws%2Fmedia%2Fcatalog%2Fproduct%2Fcache%2F1%2Fimage%2F1200x1200%2F9df78eab33525d08d6e5fb8d27136e95%2Ff%2Fi%2Ffibaro-motion-sensor.jpg&q=0&b=1&p=0&a=1)|![Tradfri Motion](https://www.ikea.com/us/en/images/products/tradfri-wireless-motion-sensor__0631744_PE695174_S5.JPG?f=s)|

#### Garden

The garden isn't automated, yet! For now, there is only a Gardena automatic watering system for our kitchen garden. A weather station is in the build with solar pannel, temperature, humidity, pressure, rain sensor, light sensor, UV sensor. Based of an ESP 1286 and an Arduino (or not, not certain for the moment)

# And that's all folks!
Thanks for reading until here!
