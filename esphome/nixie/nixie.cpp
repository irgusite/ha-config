#include "nixie.h"


byte _frame[8]; // 8 bytes = 64 bits = 4*10 bits, 24 remaining
static int last_printed_hour = 30;
static bool CPPstarted = false;
  
void nixieSetup(){
  pinMode(PIN_HV_nLATCH, OUTPUT);
  pinMode(PIN_HV_CLK, OUTPUT);
  pinMode(PIN_HV_MOSI, OUTPUT);
  resetSegments();
  updateDisplay();
}

void resetCathodePoisonningPreventionStart(){
  CPPstarted = true;
}

void cathodePoisinningPrevention(){
  if(CPPstarted){
    static unsigned long timeatstart = 0;

    static unsigned int counter = 0;
    int timeDig1[10] = {0,0,72,120,120,120,120,120,120,120};
    int timeDig2[10] = {0,0,0,0,12,12,12,12,12,12};
    int timeDig3[10] = {0,0,0,0,0,0,48,48,48,48};
    int timeDig4[10] = {0,0,0,0,0,0,0,0,0,0};
    int dig1 = -1;
    int dig2 = -1;
    int dig3 = -1;
    int dig4 = -1;

    dig1 = (timeatstart+timeDig1[counter]*1000<millis())?-1:counter;
    dig2 = (timeatstart+timeDig2[counter]*1000<millis())?-1:counter;
    dig3 = (timeatstart+timeDig3[counter]*1000<millis())?-1:counter;
    dig4 = (timeatstart+timeDig4[counter]*1000<millis())?-1:counter;
    
    nixieSetAll(dig1, dig2, dig3, dig4);
    
    if(timeatstart+timeDig1[counter]*1000<millis() && timeatstart+timeDig2[counter]*1000<millis() && timeatstart+timeDig3[counter]*1000<millis() && timeatstart+timeDig4[counter]*1000<millis()){
      counter++;
      timeatstart = millis();
    }
    if (counter == 10){
      CPPstarted = false;
      counter = 0;
    }
  }
}

void nixieShowNumber(int number){// number between 0 and 9999
  resetSegments();
  int dig4 = number/1000==0?-1:number/1000;
  int dig3 = (number%1000)/100==0?-1:(number%1000)/100;
  int dig2 = (number%100)/10;
  int dig1 = number%10;

  if(number < 0){
    dig4 = 1;
  }
  
  setHours(dig4, dig3);
  setMinutes(dig2, dig1);
  updateDisplay();
}

void nixieTestAll(){
  for(int i = 0; i<10; ++i){
    resetSegments();
    setMinutes(i,i);
    setHours(i,i);
    updateDisplay();
    delay(200);
  }
}

void nixieTurnOff(){
  resetSegments();
  updateDisplay();
  last_printed_hour = 30;
}

void nixieSetAll(int first, int second, int third, int fourth){
  resetSegments();
  setHours(first, second);
  setMinutes(third, fourth);
  updateDisplay();
}

void updateDisplay(){
  digitalWrite(PIN_HV_nLATCH, LOW);
  
  for(int i = 0; i<8 ; ++i){
    shiftOut(PIN_HV_MOSI, PIN_HV_CLK, MSBFIRST, _frame[i]);
  }
  digitalWrite(PIN_HV_nLATCH, HIGH);
}

void updateTimeWithTransition(int hours, int minutes){
  for( int j = 0; j <= 10 ; ++j){
    resetSegments();
    setMinutes(j<=minutes/10?j:minutes/10, j<=minutes%10?j:minutes%10);
    if ( hours != last_printed_hour ){
      setHours(j<=hours/10?j:hours/10, j<=hours%10?j:hours%10);
    }
    else{
      setHours(hours/10, hours%10);
    }
    updateDisplay();
    delay(200);
  }
}

void setMinutes(int ten, int unit){
  // units are from bit 17 to 26
  if (unit == -1){
    //turn off the display
  }
  else if(unit < 8){
    _frame[5] = (1 << unit);
  }
  else{
    _frame[4] = (1 << (unit-8));
  }
  if (ten == -1){
    //turn off the display
  }
  else if(ten < 8){
    _frame[7] = (1 << ten);
  }
  else{
    _frame[6] = (1 << (ten-8));
  }
}

void setHours(int ten, int unit){
  // units are from bit 17 to 26
  if (unit == -1){
    //turn off the display
  }
  else if(unit < 8){
    _frame[1] = (1 << unit);
  }
  else{
    _frame[0] = (1 << (unit-8));
  }
  if (ten == -1){
    //turn off the display
  }
  else if(ten < 8){
    _frame[3] = (1 << ten);
  }
  else{
    _frame[2] = (1 << (ten-8));
  }
}

void resetSegments(){
  for(int i = 0; i<8 ; ++i){
    _frame[i] = 0b00000000;
  }
}
