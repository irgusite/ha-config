#ifndef __NIXIE_H
#define __NIXIE_H

#include <ESP8266WiFi.h>

#define PIN_HV_MOSI D7
#define PIN_HV_CLK D5
#define PIN_HV_nLATCH D0


void nixieSetup();

void nixieTestAll();

void nixieTurnOff();
void nixieSetAll(int first, int second, int third, int fourth);
void updateTimeWithTransition(int hours, int minutes);
void nixieShowNumber(int number);
void cathodePoisinningPrevention();
void resetCathodePoisonningPreventionStart();

void updateDisplay();
void setMinutes(int ten, int unit);
void setHours(int ten, int unit);
void resetSegments();

#endif
